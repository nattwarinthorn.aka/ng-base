# SamStarter

This project was generated with [Angular CLI](https://github.com/angular/angular-cli) version 11.2.14.

### Upgrade to 12.2.16

### _minimum require in workspace_
> - node version 12.17.0 and up
> - npm version 6.14.4 and up


## Packages

Packages uses of this project to work properly: 

- [Angular CLI](https://github.com/angular/angular-cli) - Angular version 11.2.14 
- [NGRX](https://ngrx.io/guide/store) - NgRx is a framework for building reactive applications in Angular.
- [Translogo](https://ngneat.github.io/transloco/) - Translogo allows you to define translations for your content in multi languages.
- [PRIMENG](https://primefaces.org/primeng/showcase/#/setup) - PrimeNG is a rich set of open source native Angular UI components.
- [PrimeFlex](https://www.primefaces.org/primeflex/) - PrimeFlex is a lightweight responsive CSS utility library to accompany Prime UI libraries and static webpages as well.
- [date-fns](https://date-fns.org/docs/Getting-Started#introduction) - Modern JavaScript date utility library
- [Lodash](https://lodash.com/) - A modern JavaScript utility library delivering modularity, performance & extras.
- [Compodoc](https://compodoc.app/guides/getting-started.html) - Compodoc is a documentation tool for Angular applications. It generates a static documentation of your application.



## Installation

```sh
git clone https://gitlab.com/nattwarinthorn.aka/ng-base.git
npm i
```

## Development server

Run `npm start` for a dev server. Navigate to `http://localhost:4200/`. The app will automatically reload if you change any of the source files.

## Code scaffolding

Run `ng generate component component-name` to generate a new component. You can also use `ng generate directive|pipe|service|class|guard|interface|enum|module`.

## Build

Run `npm build` to build the project. The build artifacts will be stored in the `dist/` directory. Use the `--prod` flag for a production build.

## Running unit tests

Run `npm test` to execute the unit tests via [Karma](https://karma-runner.github.io).

## Running end-to-end tests

Run `npm e2e` to execute the end-to-end tests via [Protractor](http://www.protractortest.org/).

## Further help

To get more help on the Angular CLI use `ng help` or go check out the [Angular CLI Overview and Command Reference](https://angular.io/cli) page.
