import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormatMoneyPipe } from './pipes/format-money.pipe';
import {AvatarModule} from 'primeng/avatar';
import { TagModule } from 'primeng/tag';
// Form
import {CalendarModule} from 'primeng/calendar';
import {CheckboxModule} from 'primeng/checkbox';
import {DropdownModule} from 'primeng/dropdown';
import {InputSwitchModule} from 'primeng/inputswitch';
import {InputTextModule} from 'primeng/inputtext';
import {InputTextareaModule} from 'primeng/inputtextarea';
import {InputNumberModule} from 'primeng/inputnumber';
import {KeyFilterModule} from 'primeng/keyfilter';
import {ListboxModule} from 'primeng/listbox';
import {MultiSelectModule} from 'primeng/multiselect';
import {RadioButtonModule} from 'primeng/radiobutton';
import {SelectButtonModule} from 'primeng/selectbutton';
import {ToggleButtonModule} from 'primeng/togglebutton';
import {ButtonModule} from 'primeng/button';
// Table
import {TableModule} from 'primeng/table';
import {TreeTableModule} from 'primeng/treetable';
import {TreeNode} from 'primeng/api';
import {VirtualScrollerModule} from 'primeng/virtualscroller';
// Panel
import {AccordionModule} from 'primeng/accordion';
import {PanelModule} from 'primeng/panel';
import {TabViewModule} from 'primeng/tabview';
import {ToolbarModule} from 'primeng/toolbar';
// Dialog
import {ConfirmDialogModule} from 'primeng/confirmdialog';
import {ConfirmationService} from 'primeng/api';
import {DialogModule} from 'primeng/dialog';
import {DynamicDialogModule} from 'primeng/dynamicdialog';
import {TooltipModule} from 'primeng/tooltip';
//Menu
import {BreadcrumbModule} from 'primeng/breadcrumb';
import {MenubarModule} from 'primeng/menubar';
import {MenuModule} from 'primeng/menu';
import {MenuItem} from 'primeng/api';
// Toast
import {ToastModule} from 'primeng/toast';
import {MessagesModule} from 'primeng/messages';
import {MessageModule} from 'primeng/message';

@NgModule({
  declarations: [
    FormatMoneyPipe
  ],
  imports: [
    CommonModule,
    AvatarModule,
    TagModule,
    CalendarModule,
    CheckboxModule,
    DropdownModule,
    InputSwitchModule,
    InputTextModule,
    InputTextareaModule,
    InputNumberModule,
    KeyFilterModule,
    ListboxModule,
    MultiSelectModule,
    RadioButtonModule,
    SelectButtonModule,
    ToggleButtonModule,
    ButtonModule,
    TableModule,
    TreeTableModule,
    VirtualScrollerModule,
    AccordionModule,
    PanelModule,
    TabViewModule,
    ToolbarModule,
    ConfirmDialogModule,
    DialogModule,
    DynamicDialogModule,
    TooltipModule,
    BreadcrumbModule,
    MenubarModule,
    MenuModule,
    ToastModule,
    MessagesModule,
    MessageModule
  ],
  exports: [
    CommonModule,
    AvatarModule,
    TagModule,
    CalendarModule,
    CheckboxModule,
    DropdownModule,
    InputSwitchModule,
    InputTextModule,
    InputTextareaModule,
    InputNumberModule,
    KeyFilterModule,
    ListboxModule,
    MultiSelectModule,
    RadioButtonModule,
    SelectButtonModule,
    ToggleButtonModule,
    ButtonModule,
    TableModule,
    TreeTableModule,
    VirtualScrollerModule,
    AccordionModule,
    PanelModule,
    TabViewModule,
    ToolbarModule,
    ConfirmDialogModule,
    DialogModule,
    DynamicDialogModule,
    TooltipModule,
    BreadcrumbModule,
    MenubarModule,
    MenuModule,
    ToastModule,
    MessagesModule,
    MessageModule
  ]
})
export class SharedModule { }
