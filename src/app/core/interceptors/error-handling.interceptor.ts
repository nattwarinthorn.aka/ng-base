import { Injectable } from '@angular/core';
import {
  HttpRequest,
  HttpHandler,
  HttpEvent,
  HttpInterceptor,
  HttpErrorResponse
} from '@angular/common/http';
import { throwError, Observable, of } from 'rxjs';
import {catchError, retry} from 'rxjs/operators'
import {MessageService} from 'primeng/api';

@Injectable()
export class ErrorHandlingInterceptor implements HttpInterceptor {

  constructor(private messageService: MessageService) {}

  intercept(request: HttpRequest<any>, next: HttpHandler): Observable<HttpEvent<any>> {
    return next.handle(request).pipe(
      retry(1), catchError((error: HttpErrorResponse) => { 
        console.error(error);
        if (error.status !== 401) {
          this.messageService.add({severity:'error', summary: 'Error', detail: error.message});
        }
        return throwError(error)})
    );
  }
}
