import { Component } from '@angular/core';
import { TranslocoService } from '@ngneat/transloco';

@Component({
  selector: 'sam-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss']
})
export class AppComponent {
  title = 'samStarter';
  constructor(private translocoService: TranslocoService) { }

  ngOnInit() : void {
    this.translocoService.setDefaultLang('th');
  }
}
